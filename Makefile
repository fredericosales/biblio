# make file for C source
# Frederico Sales <frederico.sales@engenharia.ufjf.br>
# Engenharia Computacional - UFJF - 201765803B
# 2018
#
TARGET = biblio
LIBS = -lm
CC = gcc
CFLAGS = -g -Wall
RM = rm -f

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c $(LIBS)

clean:
	$(RM) $(TARGET) *.o
