/*
 * Frederico Sales <frederico.sales@engenharia.ufjf.br>
 * Engenharia Computacional - 201765803B
 * biblio.h
 * 
 */

#ifndef biblio_h
#define biblio_h

// include
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// define


// var


// object


// prototype
void copia(char*, char*);           // copia uma string para outra
int comprimento(char*);             // retorna o comprimento da string
int iguais(char*, char*);           // verifica se duas strings sao iguais
void minusculo(char*);              // transforma os caracteres em minusculo
void maiusculo(char*);              // transforma os caracteres em maiusculo
void converterInteiro(char*);       // se a string é composta por numeros converte retorna seu valor se nao retorna -1
void inicialMaiuscula(char*);       // passa o caractere inicial de cada palavra para maiusculo
void concatena(char*, char*);       // acrescenta uma string no final da outra
void procuraTrecho(char*, char*);   // procura uma string em outra
void procuraCaracter(char*, char);  // procura a ocorrencia de caracteres na string


// function

// copia uma string para outra
void copia(char origem[], char destino[]) {
    int i;
    while(origem[i] != '\0') {
        destino[i] = origem[i];
    }
    // imprime destino
}

// retorna o comprimento de um string
int comprimento(char string[]) {
    int count;
    while(string[count] != '\0') {
        count++;
    }
    return count;
}

// verifica se duas strings sao iguais
int iguais(char original[], char clone[]) {
    int i, org, clo;
    org = comprimento(original);
    clo = comprimento(clone);
    if(org == clo) {
        while(original[i] != '\0') {
            if(original[i] == clone[i]) {
                return 1;
            }
            i++;
        }
    }
    return 0;
} 

// transforma maiusculo para minusculo
void minusculo(char string[]) {
    int i;
    while(string[i] != '\0') {
        if(string[i] >= 'A' && string[i] <= 'Z') {
            string[i] = string[i] - 'a' + 'A';
            i++;
        }
    }
}

// transforma caracteres em maiusculo
void maiusculo(char string[]) {
    int i;
    while(string[i] != '\0') {
        if(string[i] >= 'a' && string[i] <= 'z') {
            string[i] = string[i] + 'a' - 'A';
            i++;
        }
    }
}

// converte a string para inteiros
void converte(char string[]) {
    int i, tam;
    int arr[comprimento(string)];

    while(string[i] != '\0') {
        if(string[i] >= '0' && string[i] <= '9') {
            arr[i] = string[i] - 48;
        }
        i++;
    }
}

// passa o caractere inicial de cada palavra para maiusculo
void inicialMaiuscula(char string) {
    /* 
     * um conjunto de caracteres justapostos -> string = {palavra}
     * se a posicao zero for != ' ' coverte para maiuscula
     * entao se for espaco converte posicao do espaco + 1 para maiuscula 
    */
}

#endif 